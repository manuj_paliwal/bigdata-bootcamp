---
layout: SpecialPage
# sidebarDepth: 2
---
# Homework

<!-- subtitle: Homwork description and submission -->

## Important dates

- Jan 20: Homework 1 due
- Feb 3: Homework 2 due
- Feb 17: Homework 3 due
- Mar 10: Homework 4 due
- Mar 24: Homework 5 due

## Release

Homework of this class will be distributed through [Canvas](http://gatech.instructure.com/).

## Discussion

For questions regarding homework, we encourage you to use piazza to raise discussion and we will answer ASAP. If necessary you can email [instructor and TAs](/contact.html). For on-campus students, we will schedule TA office hours.

## Submission

As a practical class, we will have a lot of **programming questions**. Please strictly follow submission instructions in each homework, especially naming and structure of submission. It's your responsibility to make sure your submission is compilable and runnable in standard teaching environment with provided code skeleton. Non-runnable code will directly lead to 0 score. You are not allowed to change any existing function or class names from provided skeleton code unless got permission from instructor or TA. Otherwise, your submission may fail our tests and got penalty in your score.

## COLLABORATION & GROUP WORK

Homework assignments are strictly individual efforts, while final projects can be done by small groups (3 people or less) or individuals and grading rubrics will be the same for every team. You can discuss high level concepts regarding to lectures or homework on the piazza but you shouldn't share your own (or others') solution and code with other students (either on piazza or through other means), and we will use anti-cheating software to avoid cheating.

## EXTENSIONS, LATE ASSIGNMENTS, & RE-SCHEDULED/MISSED EXAMS

Each student is allowed 2 days of late submission, to be USED for homework. You can use these 2 days for any assignment. Once you have used up your late days, late assignments will be penalized at a rate of 10% per day. Assignments more than 5 days late will not be accepted.
