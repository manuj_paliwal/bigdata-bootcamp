---
home: true
heroImage: images/favicon.png
heroText: CSE 6250
tagline: Big Data Analytics for Healthcare
# heroSubText: Big Data Analytics for Healthcare
# tagline: Georgia Tech Big Data Bootcamp training material
navbar: false
withContent: true
actionText: Get Started →
actionLink: /about.html
# sponsors:
# - title: Microsoft Azure
#   link: https://azure.microsoft.com
#   image: images/sponsors/azure-logo-large.png
# - title: Amazon Web Service
#   link: https://aws.amazon.com
#   image: images/sponsors/aws-logo-large.png
# - title: MS in Analytics @ Georgia Tech
#   link: http://analytics.gatech.edu
#   image: images/sponsors/gt-analytics-ms.png
footer: Jimeng Sun  •  2018  •  sunlab.org
---
#

<div class="main-explain-area jumbotron">

This website covers information for [Georgia Institute of Technology](//www.gatech.edu)'s Spring 2018 course **CSE6250 Big Data Analytics for Healthcare**.  All students may refer to this site for most up to date content.

+ Instructor: [Prof. Jimeng Sun](//sunlab.org)
+ Discussion: [CSE6250 Spring 2018 Piazza](http://piazza.com/gatech/spring2018/cse6250)
+ Location:   Klaus 2443
+ Time:       Tue/Thu 3-4:15PM

</div>
